//
//  MLPicture.swift
//  ImmutableModels
//
//  Created by gonzaloz on 6/12/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation

public struct MLPicture: Codable {
    public let id: String
    public let url: String
    public let secureURL: String

    enum CodingKeys: String, CodingKey {
        case id
        case url
        // As we are gonna use "convertFromSnakeCase" decode strategy I need to set the coding key for this attribute. I could have avoided this calling the attrib "secureUrl" instead of "secureURL"
        case secureURL = "secureUrl"
    }
}
