//
//  MLCountry.swift
//  ImmutableModels
//
//  Created by gonzaloz on 6/9/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation

public struct MLCountry: Codable {
    public let id: String
    public let name: String
}
