//
//  SearchResults.swift
//  ImmutableModels
//
//  Created by gonzaloz on 6/12/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation

public struct MLSearchResults: Codable {
    public let pagination: MLPagination
    public let products: [MLProduct]

    enum CodingKeys: String, CodingKey {
        case pagination = "paging"
        case products = "results"
    }
}
