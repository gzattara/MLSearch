//
//  Pagination.swift
//  ImmutableModels
//
//  Created by gonzaloz on 6/12/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation

public struct MLPagination: Codable {
    public let total: Int
    public let offset: Int
    public let limit: Int
    public let primaryResults: Int
}
