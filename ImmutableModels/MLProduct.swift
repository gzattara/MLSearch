//
//  MLProduct.swift
//  ImmutableModels
//
//  Created by gonzaloz on 6/11/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation

public struct MLProduct: Codable {
    public let id: String
    public let siteId: String
    public let title: String
    public let price: Double
    public let currencyId: String
    public let availableQuantity: Int
    public let soldQuantity: Int
    public let buyingMode: String
    public let stopTime: String
    public let condition: String
    public let thumbnail: String
    public let acceptsMercadopago: Bool
    public let sellerAddress: MLAddress
    public let categoryId: String
    public let pictures: [MLPicture]?
}

public struct MLAddress: Codable {
    public let city: MLPlace
    public let state: MLPlace?
}

public struct MLPlace: Codable {
    public let id: String
    public let name: String
}
