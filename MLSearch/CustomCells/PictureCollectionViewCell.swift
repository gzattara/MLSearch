//
//  PictureCollectionViewCell.swift
//  MLSearch
//
//  Created by gonzaloz on 6/12/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import UIKit

class PictureCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
