//
//  Utils.swift
//  MLSearch
//
//  Created by gonzaloz on 6/9/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation
import UIKit
import ImmutableModels

public class Utils {

    private static let selectedSiteKey = "selectedSize"

    static var selectedSite: String {
        set {
            UserDefaults.standard.set(newValue, forKey: selectedSiteKey)
        }
        get {
            return UserDefaults.standard.string(forKey: selectedSiteKey) ?? ""
        }
    }

    static func addressFormatter(for product: MLProduct) -> String {
        return "\(product.sellerAddress.city.name), \(product.sellerAddress.state?.name ?? "")"
    }

    static func priceFormatter(for product: MLProduct) -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale.current
        return currencyFormatter.string(from: NSNumber(value: product.price)) ?? "$\(String(format: "%.2f", product.price))"
    }
}
