//
//  CustomCells+Extensions.swift
//  MLSearch
//
//  Created by gonzaloz on 6/11/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import UIKit

protocol CellIdentifier {
    static var cellIdentifier: String { get }
    static var cellHeight: Double { get }
}

extension CellIdentifier {
    static var cellIdentifier: String {
        get {
            let className = String(describing: self)
            if let last = className.components(separatedBy: ".").last {
                return last
            } else {
                return className
            }
            
        }
    }
    
    static var cellHeight: Double {
        return 104
    }
}

extension UITableViewCell: CellIdentifier {}
extension UITableViewHeaderFooterView: CellIdentifier {}
extension UICollectionReusableView: CellIdentifier {}

extension UITableView {
    func register<T>(cell: T.Type) where T: CellIdentifier {
        register(UINib(nibName: cell.cellIdentifier, bundle: nil), forCellReuseIdentifier: cell.cellIdentifier)
    }
}

extension UICollectionView {
    func register<T>(cell: T.Type) where T: CellIdentifier {
        register(UINib(nibName: cell.cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cell.cellIdentifier)
    }
}
