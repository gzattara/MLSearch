//
//  UIColor+Extension.swift
//  MLSearch
//
//  Created by gonzaloz on 6/13/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public static var mlBlue:                UIColor { return #colorLiteral(red: 0.2, green: 0.2274509804, blue: 0.5254901961, alpha: 1) }
}

