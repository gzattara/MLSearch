//
//  MLService.swift
//  MLSearch
//
//  Created by gonzaloz on 6/9/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation
import Alamofire
import ImmutableModels

// Service layer
class MLService {

    // Base server URL
    static let serverURL = "https://api.mercadolibre.com/"

    static let searchLimit: Int = 10

    // Get ML sites (countries)
    static func getCountries(success: @escaping ([MLCountry]) -> (), failure: @escaping (Error) -> ()) {
        let url = serverURL + "sites"
        Alamofire.request(url, method: .get).responseJSON() { response in
            switch response.result {
            case .success(let JSON):
                guard let countriesArray = JSON as? NSArray else {
                    success([])
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let jsonData = try JSONSerialization.data(withJSONObject: countriesArray)
                    let countries = try decoder.decode([MLCountry].self, from: jsonData)
                    success(countries)
                } catch let error {
                    failure(error)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }

    static func searchItems(searchQuery: String, offset: Int? = nil, success: @escaping (MLSearchResults) -> (), failure: @escaping (Error) -> ()) {
        let url = serverURL + "sites/\(Utils.selectedSite)/search"
        var params = ["q":searchQuery, "limit": searchLimit] as [String : Any]

        if let offset = offset {
            params["offset"] = offset
        }
        Alamofire.request(url, method: .get, parameters: params).responseJSON() { response in
            switch response.result {
            case .success(let JSON):
                guard let resultDict = JSON as? NSDictionary else {
                    assertionFailure("This error will be shown only in debug, and it is fine because this should never happen. If it happens to be shown we are doing something wrong and we can correct it while testing")
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let jsonData = try JSONSerialization.data(withJSONObject: resultDict)
                    let searchResults = try decoder.decode(MLSearchResults.self, from: jsonData)
                    success(searchResults)
                } catch let error {
                    failure(error)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }

    static func getDetails(for product: MLProduct, success: @escaping (MLProduct) -> (), failure: @escaping (Error) -> ()) {
        let url = serverURL + "items/\(product.id)"
        Alamofire.request(url, method: .get).responseJSON() { response in
            switch response.result {
                case .success(let JSON):
                    guard let resultDict = JSON as? NSDictionary else {
                        assertionFailure("This error will be shown only in debug, and it is fine because this should never happen. If it happens to be shown we are doing something wrong and we can correct it while testing")
                        return
                    }
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let jsonData = try JSONSerialization.data(withJSONObject: resultDict)
                        let product = try decoder.decode(MLProduct.self, from: jsonData)
                        success(product)
                    } catch let error {
                        failure(error)
                }
                case .failure(let error):
                    failure(error)
            }
        }
    }

    static func getRelatedProducts(categoryId: String, success: @escaping ([MLProduct]) -> (), failure: @escaping (Error) -> ()) {
        let url = serverURL + "sites/\(Utils.selectedSite)/search"
        let params = ["category":categoryId, "limit": searchLimit, "official_store_id": "all"] as [String : Any]

        Alamofire.request(url, method: .get, parameters: params).responseJSON() { response in
            switch response.result {
            case .success(let JSON):
                guard let resultDict = JSON as? NSDictionary, let productsArray = resultDict["results"] as? NSArray else {
                    assertionFailure("This error will be shown only in debug, and it is fine because this should never happen. If it happens to be shown we are doing something wrong and we can correct it while testing")
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let jsonData = try JSONSerialization.data(withJSONObject: productsArray)
                    let product = try decoder.decode([MLProduct].self, from: jsonData)
                    success(product)
                } catch let error {
                    failure(error)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
}
