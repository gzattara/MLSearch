//
//  SearchViewController.swift
//  MLSearch
//
//  Created by gonzaloz on 6/9/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import UIKit
import ImmutableModels
import Kingfisher

protocol SearchViewControllerDelegate: class {
    func userDidSearch(searchQuery: String)
    func userDidReachEnd(searchQuery: String)
    func userDidSelectProduct(product: MLProduct)
}

class SearchViewController: UIViewController {

    // Instance attributes
    weak var delegate: SearchViewControllerDelegate?
    var totalItems: Int = 0 {
        didSet {
            totalLabel.text = "Total results: \(totalItems)"
        }
    }

    var productList: [MLProduct]? = nil {
        didSet {
            stateView.isHidden = productList?.count != 0
            tableView.reloadData()
        }
    }

    // IBOutlets
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }

    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var stateImageView: UIImageView!
    @IBOutlet weak var totalLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: ProductTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: ProductTableViewCell.cellIdentifier)
        tableView.keyboardDismissMode = .onDrag
    }

}

// MARK: UISearchViewDelegate
extension SearchViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
        delegate?.userDidSearch(searchQuery: searchText)
        searchBar.resignFirstResponder()
    }
    
}

// MARK: UITableViewDelegate
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.cellIdentifier, for: indexPath) as? ProductTableViewCell, let productsList = productList, productsList.indices.contains(indexPath.row) else { return UITableViewCell() }
        let product = productsList[indexPath.row]
        cell.nameLabel.text = product.title
        cell.priceLabel.text = Utils.priceFormatter(for: product)
        let imageURL = URL(string: product.thumbnail)
        cell.productImageView.kf.setImage(with: imageURL)
        return cell
    }

    // I'm using the below delegate method to identify when the user is close to reaching the tableView's end
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let productList = productList, indexPath.row + 2 == productList.count, let searchText = searchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            // Let the coordinator know the user is getting to the end of the list so it can get the next page
            delegate?.userDidReachEnd(searchQuery: searchText)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Let the coordinator know the user selected an item
        guard let productList = productList, productList.indices.contains(indexPath.row) else { return }
        let product = productList[indexPath.row]
        delegate?.userDidSelectProduct(product: product)
    }
}
