//
//  CountryViewController.swift
//  MLSearch
//
//  Created by gonzaloz on 6/8/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import UIKit
import ImmutableModels

protocol CountryViewControllerDelegate: class {
    func didSelectCountry(country: MLCountry)
}

class CountryViewController: UIViewController {

    weak var delegate: CountryViewControllerDelegate?
    var countries: [MLCountry] = [] {
        didSet {
            countryPicker.reloadAllComponents()
        }
    }

    @IBOutlet weak var selectCountryButton: UIButton! {
        didSet {
            selectCountryButton.layer.borderColor = UIColor.mlBlue.cgColor
            selectCountryButton.layer.borderWidth = 1
            selectCountryButton.layer.cornerRadius = 18
            selectCountryButton.backgroundColor = UIColor.mlBlue
            selectCountryButton.setTitleColor(.white, for: .normal)
            selectCountryButton.setTitle("Continue", for: .normal)
        }
    }

    @IBOutlet weak var countryPicker: UIPickerView! {
        didSet {
            countryPicker.delegate = self
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "Please Select your country"
            titleLabel.textColor = UIColor.mlBlue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func selectCountryButtonTapped(_ sender: Any) {
        let index = countryPicker.selectedRow(inComponent: 0)
        guard countries.indices.contains(index) else { return }
        delegate?.didSelectCountry(country: countries[index])
    }
}

// MARK: UIPickerViewDelegate

extension CountryViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row].name
    }
}
