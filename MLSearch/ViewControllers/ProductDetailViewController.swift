//
//  ProductDetailViewController.swift
//  MLSearch
//
//  Created by gonzaloz on 6/12/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import UIKit
import ImmutableModels

protocol ProductDetailViewControllerDelegate: class {
    func userDidSelectRelatedProduct(product: MLProduct)
}

class ProductDetailViewController: UIViewController {

    weak var delegate: ProductDetailViewControllerDelegate?

    var pictures: [MLPicture]? = nil {
        didSet {
            picturesCollectionView.reloadData()
        }
    }

    var relatedProducts: [MLProduct]? = nil {
        didSet {
            relatedCollectionView.reloadData()
            guard let relatedProducts = relatedProducts else {
                relatedCollectionView.isHidden = true
                return
            }
            relatedCollectionView.isHidden = relatedProducts.count == 0
        }
    }

    @IBOutlet weak var picturesCollectionView: UICollectionView! {
        didSet {
            picturesCollectionView.delegate = self
            picturesCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.text = "Description"
        }
    }
    @IBOutlet weak var descriptionTextView: UITextView! {
        didSet {
            descriptionTextView.isEditable = false
            descriptionTextView.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var relatedCollectionView: UICollectionView! {
        didSet {
            relatedCollectionView.delegate = self
            relatedCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var relatedTitleLabel: UILabel! {
        didSet {
            relatedTitleLabel.text = "Related Items"
        }
    }
    @IBOutlet weak var mercadoPagoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up layout for pictures collection view
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.bounds.size.width - 40, height: picturesCollectionView.bounds.size.height - 20)
        layout.scrollDirection = .horizontal
        picturesCollectionView.setCollectionViewLayout(layout, animated: true)

        let relatedCVLayout = UICollectionViewFlowLayout()
        relatedCVLayout.itemSize = CGSize(width: self.view.bounds.size.width - 20, height: picturesCollectionView.bounds.size.height - 20)
        relatedCVLayout.scrollDirection = .horizontal
        relatedCollectionView.setCollectionViewLayout(relatedCVLayout, animated: true)

        // Register cell views
        picturesCollectionView.register(cell:PictureCollectionViewCell.self)
        relatedCollectionView.register(cell: RelatedProductCollectionViewCell.self)

        //Hide related products collection view until we get the data
        relatedCollectionView.isHidden = true
    }

}

extension ProductDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == picturesCollectionView {
            return pictures?.count ?? 0
        } else if collectionView == relatedCollectionView {
            return relatedProducts?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == picturesCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PictureCollectionViewCell.cellIdentifier, for: indexPath) as? PictureCollectionViewCell, let pictures = pictures, pictures.indices.contains(indexPath.row) else { return UICollectionViewCell() }
            let picture = pictures[indexPath.row]
            cell.productImageView.kf.setImage(with: URL(string: picture.secureURL))
            return cell
        } else if collectionView == relatedCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RelatedProductCollectionViewCell.cellIdentifier, for: indexPath) as? RelatedProductCollectionViewCell, let relatedProducts = relatedProducts, relatedProducts.indices.contains(indexPath.row) else { return UICollectionViewCell() }
            let product = relatedProducts[indexPath.row]
            cell.productImageView.kf.setImage(with: URL(string: product.thumbnail))
            cell.titleLable.text = product.title
            cell.priceLabel.text = Utils.priceFormatter(for: product)
            return cell
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == relatedCollectionView {
            guard let relatedProducts = relatedProducts, relatedProducts.indices.contains(indexPath.row) else { return }
            let product = relatedProducts[indexPath.row]
            delegate?.userDidSelectRelatedProduct(product: product)
        }
        return
    }
}

extension ProductDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 40, 0, 40)
    }
}
