//
//  AppCoordinator.swift
//  MLSearch
//
//  Created by gonzaloz on 6/8/18.
//  Copyright © 2018 gonzaloz. All rights reserved.
//

import Foundation
import UIKit
import ImmutableModels

//
// This class has the responsibility to handle the app flow, fetching data from the server, handling that data and provide the VCs what they need to build the UI
// it also knows how to react to user's interaction
//
class AppCoordinator: NSObject {

    private var countryViewController: CountryViewController? = nil
    private var searchViewController: SearchViewController? = nil

    private var searchingMore = false
    private var searchPagination: MLPagination?
    private var paginated: Int = 0

    private var navViewController: UINavigationController?

    // start method for the coordinator. Configures the first step in the flow handled by it
    func start(with appWindow: UIWindow) {
        // Setting up the first view controller the app will always show
        countryViewController = CountryViewController()
        countryViewController?.delegate = self
        // I can force the unwraping here since I just set the value two lines above
        let navController = UINavigationController(rootViewController: countryViewController!)
        navViewController = navController

        appWindow.rootViewController = navController
        appWindow.makeKeyAndVisible()
        getCountries()
    }

    func displaySpinner() -> UIView? {
        guard let navViewController = navViewController, let onView = navViewController.topViewController?.view else { return nil }
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }

    // get ML sites
    private func getCountries() {
        MLService.getCountries(success: { [unowned self] results in
            self.countryViewController?.countries = results
        }, failure: { error in
            print("There was an error fetching the sites: \(error)")
        })
    }

    // Show a product detail screen. Fetch everything needed
    private func showDetail(for product: MLProduct) {
        let spinnerView = displaySpinner()
        MLService.getDetails(for: product, success: { result in
            spinnerView?.removeFromSuperview()
            let productDetailVC = ProductDetailViewController(nibName: "ProductDetailViewController", bundle: nil)
            let _ = productDetailVC.view
            productDetailVC.delegate = self
            productDetailVC.pictures = result.pictures
            productDetailVC.priceLabel.text = Utils.priceFormatter(for: product)
            productDetailVC.titleLabel.text = product.title
            productDetailVC.cityLabel.text = Utils.addressFormatter(for: product)
            productDetailVC.mercadoPagoView.isHidden = !product.acceptsMercadopago

            // Get the related products by category
            // As 'hot_items' and 'trends' endpoints were not returning anything I'm using the 'officialStore' endpoint.
            // This is just to make a point on what I wanted to achieve
            MLService.getRelatedProducts(categoryId: product.categoryId, success: { [unowned productDetailVC] relatedProductsResult in
                let relatedProdcuts = relatedProductsResult.filter { $0.id != product.id }
                productDetailVC.relatedProducts = relatedProdcuts
                }, failure: { error in
                    print("In this case I'm not that worried about this error since this is an extra in the product details view, I will just ignore it")
            })
            self.navViewController?.pushViewController(productDetailVC, animated: true)
        }, failure: { error in
            spinnerView?.removeFromSuperview()
            print(error)
        })
    }
}

// MARK: CountryViewControllerDelegate
extension AppCoordinator: CountryViewControllerDelegate {

    func didSelectCountry(country: MLCountry) {
        Utils.selectedSite = country.id
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        let _ = searchVC.view
        searchVC.delegate = self
        searchViewController = searchVC

        self.navViewController?.pushViewController(searchVC, animated: true)
    }
}

extension AppCoordinator: SearchViewControllerDelegate {

    // Search products
    func userDidSearch(searchQuery: String) {
        searchPagination = nil
        let spinnerView = displaySpinner()
        MLService.searchItems(searchQuery: searchQuery, success: { [unowned self] result in
            spinnerView?.removeFromSuperview()
            guard let searchVC = self.searchViewController else { return }
            searchVC.productList = result.products
            searchVC.totalItems = result.pagination.total
            self.searchPagination = result.pagination
            }, failure: { error in
                spinnerView?.removeFromSuperview()
                guard let searchVC = self.searchViewController else { return }
                searchVC.productList = nil
                searchVC.totalItems = 0
                searchVC.stateImageView.image = #imageLiteral(resourceName: "search_error")
                searchVC.stateView.isHidden = false
        })
    }

    // Get next page for user's search
    func userDidReachEnd(searchQuery: String) {
        // There is no point on trying to get more items if we don't have the pagination object
        guard let searchPagination = searchPagination else { return }
        // Set the offset
        paginated += 1
        let offset = searchPagination.limit * paginated

        MLService.searchItems(searchQuery: searchQuery, offset: offset, success: { results in
            guard let searchVC = self.searchViewController, let searchProductList = searchVC.productList else { return }
            searchVC.productList = searchProductList + results.products
            self.searchPagination = results.pagination
        }, failure: { error in
            guard let searchVC = self.searchViewController else { return }
            searchVC.productList = nil
            searchVC.totalItems = 0
            searchVC.stateImageView.image = #imageLiteral(resourceName: "search_error")
            searchVC.stateView.isHidden = false
        })
    }

    func userDidSelectProduct(product: MLProduct) {
        showDetail(for: product)
    }
}

extension AppCoordinator: ProductDetailViewControllerDelegate {
    func userDidSelectRelatedProduct(product: MLProduct) {
        showDetail(for: product)
    }
}
